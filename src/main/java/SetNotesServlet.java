import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerApi;
import com.mongodb.ServerApiVersion;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "SetNotesServlet", value = "/set-notes")
public class SetNotesServlet extends HttpServlet {
    String connectionString = "mongodb+srv://heng:redacted@cluster0.s1rnfta.mongodb.net/?retryWrites=true&w=majority";
    ServerApi serverApi = ServerApi.builder()
            .version(ServerApiVersion.V1)
            .build();
    MongoClientSettings settings = MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString(connectionString))
            .serverApi(serverApi)
            .build();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String relationId = request.getParameter("id");
        try (MongoClient client = MongoClients.create(settings)) {
            MongoDatabase db = client.getDatabase("nutriscan");
            MongoCollection<Document> result = db.getCollection("relations");
            JSONArray arr = new JSONArray();
            try (MongoCursor<Document> c = result.find(new Document("relationId", relationId)).iterator()) {
                while (c.hasNext()) {
                    Document orig = c.next();
                    Document n = new Document();
                    n.put("note", request.getParameter("note"));
                    n.put("relationId", relationId);
                    n.put("physicianId", orig.getString("physicianId"));
                    n.put("clientId", orig.getString("clientId"));
                    n.put("name", orig.getString("name"));

                    result.replaceOne(orig, n);
                }
            }
            response.getWriter().close();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}