import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerApi;
import com.mongodb.ServerApiVersion;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "GetNotesServlet", value = "/notes")
public class GetNotesServlet extends HttpServlet {
    String connectionString = "mongodb+srv://heng:redacted@cluster0.s1rnfta.mongodb.net/?retryWrites=true&w=majority";
    ServerApi serverApi = ServerApi.builder()
            .version(ServerApiVersion.V1)
            .build();
    MongoClientSettings settings = MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString(connectionString))
            .serverApi(serverApi)
            .build();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String  userId = (String) request.getSession().getAttribute("userId");

        try (MongoClient client = MongoClients.create(settings)) {
            MongoDatabase db = client.getDatabase("nutriscan");
            MongoCollection<Document> result = db.getCollection("relations");
            StringBuilder allText = new StringBuilder();
            try (MongoCursor<Document> c = result.find(new Document("clientId", userId)).iterator()) {
                while (c.hasNext()) {
                    try {allText.append(c.next().getString("note"));} catch (Exception e){}
                }
            }

            response.setContentType("application/json");
            response.getWriter().println(new JSONObject().put("text", allText.toString().replaceAll("null","")));;
            response.getWriter().close();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}