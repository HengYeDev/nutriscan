import com.auth0.jwt.JWT;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerApi;
import com.mongodb.ServerApiVersion;
import com.mongodb.client.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.UUID;

@WebServlet(name = "AddClientServlet", value = "/add-client")
public class AddClientServlet extends HttpServlet {
    String connectionString = "mongodb+srv://heng:redacted@cluster0.s1rnfta.mongodb.net/?retryWrites=true&w=majority";
    ServerApi serverApi = ServerApi.builder()
            .version(ServerApiVersion.V1)
            .build();
    MongoClientSettings settings = MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString(connectionString))
            .serverApi(serverApi)
            .build();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (MongoClient client = MongoClients.create(settings)) {
            MongoDatabase db = client.getDatabase("nutriscan");
            MongoCollection<Document> result = db.getCollection("users");

            try (MongoCursor<Document> c = result.find(new Document("code", request.getParameter("code"))).iterator()) {
                if (c.hasNext()) {
                    MongoCollection<Document> relations = db.getCollection("relations");
                    Document d = new Document();
                    d.put("physicianId", request.getSession().getAttribute("userId"));
                    d.put("clientId", c.next().getString("userId"));
                    d.put("name", request.getParameter("name"));
                    String uuid = UUID.randomUUID().toString();
                    d.put("relationId", uuid);

                    relations.insertOne(d);
                    response.setContentType("application/json");
                    response.getWriter().println(new JSONObject().put("rid", uuid).toString());
                    response.getWriter().close();

                } else {
                    response.sendError(400);
                }
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}