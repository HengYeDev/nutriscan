import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerApi;
import com.mongodb.ServerApiVersion;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "Get", value = "/get")
public class GetServlet extends HttpServlet {
    String connectionString = "mongodb+srv://heng:redacted@cluster0.s1rnfta.mongodb.net/?retryWrites=true&w=majority";
    ServerApi serverApi = ServerApi.builder()
            .version(ServerApiVersion.V1)
            .build();
    MongoClientSettings settings = MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString(connectionString))
            .serverApi(serverApi)
            .build();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("user");
        if (userId == null || userId.isEmpty()) {
            userId = (String) request.getSession().getAttribute("userId");
            System.out.println(userId);
        }

        try (MongoClient client = MongoClients.create(settings)) {
            MongoDatabase db = client.getDatabase("nutriscan");
            MongoCollection<Document> result = db.getCollection("data");
            JSONArray arr = new JSONArray();
            try (MongoCursor<Document> c = result.find(new Document("user", userId)).iterator()) {
                while (c.hasNext()) {
                    arr.put(new JSONObject(c.next().toJson()));
                }
            }
            response.setContentType("application/json");
            response.getWriter().println(arr.toString());;
            response.getWriter().close();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}