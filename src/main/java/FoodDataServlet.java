import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerApi;
import com.mongodb.ServerApiVersion;
import com.mongodb.client.*;
import com.mongodb.client.result.InsertOneResult;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.FormBodyPartBuilder;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@WebServlet(name = "FoodDataServlet", value = "/food-data")
@MultipartConfig
public class FoodDataServlet extends HttpServlet {
    String connectionString = "mongodb+srv://heng:redacted@cluster0.s1rnfta.mongodb.net/?retryWrites=true&w=majority";
    ServerApi serverApi = ServerApi.builder()
            .version(ServerApiVersion.V1)
            .build();
    MongoClientSettings settings = MongoClientSettings.builder()
            .applyConnectionString(new ConnectionString(connectionString))
            .serverApi(serverApi)
            .build();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mealType = request.getParameter("mealType");
        Part filePart = request.getPart("picture");


        String apiUrl = "https://api.cloudflare.com/client/v4/accounts/8dfa741d9ca5e6ad2fb212f08f5fc7fe/images/v1";
        String apiToken = request.getServletContext().getInitParameter("com.cloudflare.apiKey");
        String openAiToken = request.getServletContext().getInitParameter("com.openai.key");

        // Create HttpClient
        HttpClient httpClient = HttpClients.createDefault();

        // Build the multipart request
        HttpPost httpPost = new HttpPost(apiUrl);
        httpPost.addHeader("Authorization", "Bearer " + apiToken);

        HttpEntity entity = MultipartEntityBuilder
                .create()
                .addBinaryBody("file", IOUtils.toByteArray(filePart.getInputStream()))
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                .build();

        httpPost.setEntity(entity);

        // Execute the request
        HttpResponse resp = httpClient.execute(httpPost);
        JSONObject obj = new JSONObject(IOUtils.toString(resp.getEntity().getContent(), StandardCharsets.UTF_8));
        System.out.println("OBJ" + obj);
        String imgUrl = obj.getJSONObject("result").getJSONArray("variants").getString(0);

        HttpPost openAi = new HttpPost("https://api.openai.com/v1/chat/completions");
        openAi.addHeader("Authorization", "Bearer " + openAiToken);;
        openAi.setHeader("Content-Type", "application/json");

        JSONObject requestObj = new JSONObject()
                .put("model", "gpt-4-vision-preview")
                .put("max_tokens", 500)
                .put("temperature", 0.7)
                .put("messages", new JSONArray()
                        .put(new JSONObject()
                                .put("role", "system")
                                .put("content", new JSONArray().put(new JSONObject()
                                        .put("type", "text")
                                        .put("text", "You will respond only with a JSON object, without markdown formatting, without any comments. You will determine the type of food, the approximate amount of calories, as well as approximate amount in grams of protein, carbohydrates, fats, vitamins, sodium, and minerals in the image, only with numbers, through estimation of the image contents. Assume typical food items." +
                                        "Example response with fake data: {\n" +
                                                "\"food\": \"Slice of pizza\"" +
                                                "\"calories\": 200" +
                                                "\"protein\": 50,\n" +
                                                "\"carbohydrates\": 50,\n" +
                                                "\"fats\": 50,\n" +
                                                "\"vitamins\": 50,\n" +
                                                "\"sodium\": 50,\n" +
                                                "\"minerals\": 50\n" +
                                        "}"))))
                        .put(new JSONObject()
                                .put("role", "user")
                                .put("content", new JSONArray().put(new JSONObject()
                                        .put("type", "image_url")
                                        .put("image_url", new JSONObject().put("url", imgUrl))))
                       )
                );

        openAi.setEntity(new StringEntity(requestObj.toString()));
        HttpResponse aiResp = httpClient.execute(openAi);
        String json = new JSONObject(IOUtils.toString(aiResp.getEntity().getContent(), StandardCharsets.UTF_8)).getJSONArray("choices").getJSONObject(0).getJSONObject("message").getString("content");
        System.out.println(json);
        JSONObject r = new JSONObject(json);
        r.put("user", request.getSession().getAttribute("userId"));
        r.put("timestamp", System.currentTimeMillis());
        r.put("img", imgUrl);
        response.setContentType("application/json");
        response.getWriter().print(r.toString());
        try (MongoClient client = MongoClients.create(settings)) {
            MongoDatabase db = client.getDatabase("nutriscan");
            InsertOneResult result = db.getCollection("data").insertOne(Document.parse(r.toString()));

            MongoCollection<Document> r1 = db.getCollection("daily");
            Document dateSearch = new Document();
            dateSearch.put("date", new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
            dateSearch.put("userId", request.getSession().getAttribute("userId"));
            try (MongoCursor<Document> c = r1.find(dateSearch).iterator()) {
                if (c.hasNext()) {
                    System.out.println("found");
                    Document old = c.next();
                    Document d = new Document();
                    System.out.println(old.toJson());
                    System.out.println(old.getInteger("calories"));
                    System.out.println(r.getInt("calories"));
                    d.put("calories", old.getInteger("calories") + r.getInt("calories"));
                    d.put("protein", old.getInteger("protein") + r.getInt("protein"));
                    d.put("carbohydrates", old.getInteger("carbohydrates") + r.getInt("carbohydrates"));
                    d.put("fats", old.getInteger("fats") + r.getInt("fats"));
                    d.put("vitamins", old.getInteger("vitamins") + r.getInt("vitamins"));
                    d.put("minerals", old.getInteger("minerals") + r.getInt("minerals"));
                    d.put("sodium", old.getInteger("sodium") + r.getInt("sodium"));
                    d.put("userId", request.getSession().getAttribute("userId"));
                    d.put("timestamp", System.currentTimeMillis());
                    d.put("date", new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
                    r1.replaceOne(old, d);
                } else {
                    System.out.println("inserting");
                    Document d = new Document();
                    d.put("calories", r.getInt("calories"));
                    d.put("protein", r.getInt("protein"));
                    d.put("carbohydrates",  r.getInt("carbohydrates"));
                    d.put("fats", r.getInt("fats"));
                    d.put("vitamins",  r.getInt("vitamins"));
                    d.put("minerals", r.getInt("minerals"));
                    d.put("sodium",  r.getInt("sodium"));
                    d.put("userId", request.getSession().getAttribute("userId"));
                    d.put("date", new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
                    d.put("timestamp", System.currentTimeMillis());
                    r1.insertOne(d);
                }
            }
            response.getWriter().close();
            System.out.println("==");

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}