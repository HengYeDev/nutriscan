<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.userway.org/widget.js" data-account="n8rLcGf9QH"></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content="NutriScan"/>
    <title>NutriScan - The next generation of nutrient management</title>
    <!-- Favicon--><!-- Bootstrap icons-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="/css/styles.css" rel="stylesheet"/>
</head>
<body class="d-flex flex-column h-100">
<main class="flex-shrink-0">
    <!-- Navigation-->
    <%@ include file="jspf/nav.jspf" %>
    <!-- Header-->
    <header class="bg-success py-5">
        <div class="container px-5">
            <div class="row gx-5 align-items-center justify-content-center">
                <div class="col-lg-8 col-xl-7 col-xxl-6">
                    <div class="my-5 text-center text-xl-start">
                        <h1 class="display-5 text-white mb-2 fw-bolder">A picture of your meal is all you need</h1>
                        <p class="lead fw-normal text-white-50 mb-4">The next generation of nutrient management</p>
                        <div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start">
                            <a class="btn btn-outline-light btn-lg px-4 me-sm-3" href="/login">Try it now <span class="bi bi-arrow-right"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-xxl-6 d-none d-xl-block text-center"><img class="img-fluid rounded-3 my-5"
                                                                                   src="big.png"
                                                                                   alt="Picture of a robot painting"/>
                </div>
            </div>
        </div>
    </header>
    <!-- Features section-->
    <section class="py-5" id="features">
        <div class="container px-5 my-5">
            <div class="row gx-5">
                <div class="col-lg-4 mb-5 mb-lg-0"><h2 class="fw-bolder mb-0">A new way to track diet</h2>
                <h5 class="text-muted mt-4">The next generation of dietary health, powered by the newest innovations in artificial intelligence. Try it today for instant results!</h5></div>
                <div class="col-lg-8">
                    <div class="row gx-5 row-cols-1 row-cols-md-2">
                        <div class="col mb-5 h-100">
                            <div class="feature bg-success bg-gradient text-white rounded-3 mb-3"><i
                                    class="bi bi-emoji-smile"></i></div>
                            <h2 class="h5">Simple</h2>
                            <p class="mb-0">Accessible from any device, just snap a photo of your meal and let it do the work, delivering instant and accurate macronutrient information in real-time. No more guesswork, just quick and convenient tracking of your nutritional intake.</p>
                        </div>
                        <div class="col mb-5 h-100">
                            <div class="feature bg-success bg-gradient text-white rounded-3 mb-3"><i
                                    class="bi bi-robot"></i></div>
                            <h2 class="h5">Cutting-edge</h2>
                            <p class="mb-0">This application harnesses the power of AI to recognize food in the real world, making it easier than ever to track your dietary intake. By leveraging computer vision and AI, it not only detects and records the foods you consume but also creates a comprehensive history for you to review and analyze.</p>
                        </div>
                        <div class="col mb-5 h-100">
                            <div class="feature bg-success bg-gradient text-white rounded-3 mb-3"><i
                                    class="bi bi-file-bar-graph"></i></div>
                            <h2 class="h5">Seamless record-keeping</h2>
                            <p class="mb-0">This app facilitates direct, real-time medical guidance between doctors and patients via secure messaging. Doctors can upload personalized advice, while patients input their food choices for nutritional insights. This seamless communication promotes better health outcomes and informed decisions.</p>
                        </div>
                        <div class="col mb-5 h-100">
                            <div class="feature bg-success bg-gradient text-white rounded-3 mb-3"><i
                                    class="bi bi-eye-slash"></i></div>
                            <h2 class="h5">Secure</h2>
                            <p class="mb-0">All your data is secured in a secure MongoDB database and protected by advanced authentication protocols powered by Auth0.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Testimonial section-->
    <div class="py-5 bg-light">
        <div class="container px-5 my-5">

        </div>
    </div>
</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

<!-- Footer-->
<%@ include file="jspf/footer.jspf" %>
</body>
</html>
