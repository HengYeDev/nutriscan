<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    if (request.getSession().getAttribute("userId") == null) {
        response.sendRedirect("/login");
    }
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.userway.org/widget.js" data-account="n8rLcGf9QH"></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content="NutriScan"/>
    <title>Account Information - NutriScan</title>
    <!-- Favicon--><!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body class="d-flex flex-column h-100">
<main class="flex-shrink-0">
    <!-- Navigation-->
    <%@ include file="jspf/nav.jspf" %>
    <!-- Header-->
    <header class="bg-success py-3">
        <div class="container px-5">
            <h1 class="text-light">Welcome</h1>
        </div>
    </header>
    <!-- Features section-->
    <section class="container px-5 mt-4">

        <div class="row">
            <div class="col-md-7">
                <h3>Today's intake</h3>
                <br>
                <h5>Calories: <span id="todayCalories">0</span></h5>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Nutrient</th>
                        <th scope="col">Amount (g)</th>
                        <th scope="col">Progress</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <th scope="row">Protein</th>
                        <td id="todayProtein">0</td>
                        <td><div class="progress">
                            <div class="progress-bar" id="proteinBar" role="progressbar" aria-label="Example with label" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div></td>
                    </tr>
                    <tr>
                        <th scope="row">Carbohydrates</th>
                        <td id="todayCarbohydrates">0</td>
                        <td><div class="progress">
                            <div class="progress-bar" id="carbBar" role="progressbar" aria-label="Example with label" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div></td>
                    </tr>
                    <tr>
                        <th scope="row">Fats</th>
                        <td id="todayFats">0</td>
                        <td><div class="progress">
                            <div class="progress-bar" id="fatBar" role="progressbar" aria-label="Example with label" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div></td>
                    </tr>
                    <tr>
                        <th scope="row">Vitamins</th>
                        <td id="todayVitamins">0</td>
                        <td><div class="progress">
                            <div class="progress-bar" id="vitaminBar" role="progressbar" aria-label="Example with label" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div></td>
                    </tr>
                    <tr>
                        <th scope="row">Minerals</th>
                        <td id="todayMinerals">0</td>
                        <td><div class="progress">
                            <div class="progress-bar" id="mineralBar" role="progressbar" aria-label="Example with label" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div></td>
                    </tr>
                    <tr>
                        <th scope="row">Sodium</th>
                        <td id="todaySodium">0</td>
                        <td><div class="progress">
                            <div class="progress-bar" id="sodiumBar" role="progressbar" aria-label="Example with label" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                        </div></td>
                    </tbody>
                </table>
            </div>
            <div class="col-md-5">
                <div id="doctor"></div>
                <a class="btn btn-success btn-lg w-100" href="scan.jsp">Scan Now</a>
                <hr>
                <p>NutriScan ID: <span class="font-monospace"><%= session.getAttribute("code") %></span></p>
                <hr>
                <h4>Today's food</h4>
                <div id="todayImgs">None yet...</div>
            </div>
        </div>
        <hr>
        <h3>History</h3>
        <hr>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#" id="dBtn">Daily intake</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" id="fBtn">Food history</a>
            </li>

        </ul>
        <div class="dailyIntake">
            <table class="table">
                <thead>
                <tr>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody id="daily">
                </tbody>
            </table>
        </div>
        <div class="foodHistory">
            <table class="table">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Food</th>
                </tr>
                </thead>
                <tbody id="tbl">
                </tbody>
            </table>
        </div>

    </section>

</main>
<!-- Footer-->
<footer class="bg-dark py-4 mt-auto">
    <div class="container px-5">
        <div class="row align-items-center justify-content-between flex-column flex-sm-row">
            <div class="col-auto">
                <div class="small m-0 text-white">Copyright &copy; NutriScan 2024</div>
            </div>
            <div class="col-auto">
                <a class="link-light small" href="#!">Privacy</a>
                <span class="text-white mx-1">&middot;</span>
                <a class="link-light small" href="#!">Terms</a>
                <span class="text-white mx-1">&middot;</span>
                <a class="link-light small" href="#!">Contact</a>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="detailsModal" tabindex="-1" aria-labelledby="detailsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailsModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-8">
                        <h5>Calories: <span id="calories"></span></h5>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Nutrient</th>
                                <th scope="col">Amount (g)</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <th scope="row">Protein</th>
                                <td id="protein"></td>
                            </tr>
                            <tr>
                                <th scope="row">Carbohydrates</th>
                                <td id="carbohydrates"></td>
                            </tr>
                            <tr>
                                <th scope="row">Fats</th>
                                <td id="fats"></td>
                            </tr>
                            <tr>
                                <th scope="row">Vitamins</th>
                                <td id="vitamins"></td>
                            </tr>
                            <tr>
                                <th scope="row">Minerals</th>
                                <td id="minerals"></td>
                            </tr>
                            <tr>
                                <th scope="row">Sodium</th>
                                <td id="sodium"></td>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <img class="w-100" id="image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script src="dateformat.js"></script>
<script>
    var globData;
    var dailyTotals;

    function sameDay(d1, d2) {
        return d1.getFullYear() === d2.getFullYear() &&
            d1.getMonth() === d2.getMonth() &&
            d1.getDate() === d2.getDate();
    }

    $.get('/notes', function(data) {
        if (data.text && !data.text.includes("null")) {
            $('#doctor').show();
            $('#doctor').html('<h4>Note from your healthcare provider</h4>' + data.text);
        }
    })

    $.get('/get', function (data) {
        data.sort(function (b, a) {
            return parseFloat(a.timestamp) - parseInt(b.timestamp);
        });
        globData = data;


        for (var i = 0; i < data.length; i++) {
            try {
                var d = new Date(data[i].timestamp);
                $('#tbl').append('<tr><td>' + d.customFormat("#MM#/#DD#/#YYYY# #hh#:#mm#") + '</td><td>' + data[i].food + '</td><td><button type="button" class="details btn btn-success" data-id=' + i + '>Details</button></td></tr>');

                if (sameDay(d, new Date())) {
                    if ($('#todayImgs').html() === "None yet...") {
                        $('#todayImgs').html("");
                    }
                    $('#todayImgs').append('<img src="' + data[i].img + '" style="width: 150px" class="m-3">');
                }
            } catch (e) {
                console.log(e);
            }
        }

        $('.details').click(function () {
            var id = $(this).data("id");
            console.log(id);
            $('#detailsModalLabel').text(globData[id].food);
            $('#protein').text(globData[id].protein);
            $('#carbohydrates').text(globData[id].carbohydrates);
            $('#fats').text(globData[id].fats);
            $('#vitamins').text(globData[id].vitamins);
            $('#minerals').text(globData[id].minerals);
            $('#calories').text(globData[id].calories);
            $('#sodium').text(globData[id].sodium);
            $('#image').prop("src", globData[id].img);
            $('#detailsModal').modal('show');
        });
    });
    var dailies;
    $.get("/daily",
        function (data) {
        dailies = data;
            for (var i = 0; i < data.length; i++) {
                console.log(data[i]);
                $('#daily').append("<tr><td>" + data[i].date + '</td><td><button type="button" class="btn btn-success dailyBtn" data-id="' + i + '">View Breakdown</button></tr>');

                if (data[i].date === new Date().customFormat("#MM#/#DD#/#YYYY#")) {
                    $('#todayProtein').text(data[i].protein);
                    var proteinAmt = (parseFloat(data[i].protein) / 50) * 100;
                    $('#proteinBar').prop("style", "width: " + proteinAmt + "% !important");
                    $('#proteinBar').html(Math.round(proteinAmt) + "%");

                    $('#todayCarbohydrates').text(data[i].carbohydrates);
                    var carbAmount = (parseFloat(data[i].carbohydrates) / 310) * 100;
                    $('#carbBar').prop("style", "width: " + carbAmount + "% !important");
                    $('#carbBar').html(Math.round(carbAmount) + "%");

                    $('#todayFats').text(data[i].fats);
                    var fatAmt = (parseFloat(data[i].fats) / 70) * 100;
                    $('#fatBar').prop("style", "width: " + fatAmt + "% !important");
                    $('#fatBar').html(Math.round(fatAmt) + "%");

                    $('#todayVitamins').text(data[i].vitamins);
                    var vitAmt = (parseFloat(data[i].vitamins) / 70) * 100;
                    $('#vitaminBar').prop("style", "width: " + vitAmt + "% !important");
                    $('#vitaminBar').html(Math.round(vitAmt) + "%");

                    $('#todayMinerals').text(data[i].minerals);
                    var minAmt = (parseFloat(data[i].minerals) / 20) * 100;
                    $('#minAmt').prop("style", "width: " + minAmt + "% !important");
                    $('#minAmt').html(Math.round(minAmt) + "%");

                    $('#todayCalories').text(data[i].calories);

                    $('#todaySodium').text(parseFloat(data[i].sodium)/1000.0);
                    var t = ((parseFloat(data[i].sodium)/1000) / 4) * 100;
                    $('#sodiumBar').prop("style", "width: " + t + "% !important");
                    $('#sodiumBar').html(Math.round(t) + "%");
                }
            }

            $('.dailyBtn').on("click", function () {
                var id = $(this).data("id");
                console.log(id);
                $('#detailsModalLabel').text("Daily intake for " + dailies[id].date);
                $('#protein').text(dailies[id].protein);
                $('#carbohydrates').text(dailies[id].carbohydrates);
                $('#fats').text(dailies[id].fats);
                $('#vitamins').text(dailies[id].vitamins);
                $('#minerals').text(dailies[id].minerals);
                $('#calories').text(dailies[id].calories);
                $('#sodium').text(dailies[id].sodium);
                $('#image').prop("src", dailies[id].img);
                $('#detailsModal').modal('show');
            });

        });
</script>
<script>
    $('.foodHistory').hide();
    $('#dBtn').click(function () {
        $('#dBtn').addClass("active");
        $('#fBtn').removeClass("active");
        $('.foodHistory').hide();
        $('.dailyIntake').show();
    });
    $('#fBtn').click(function () {
        $('#fBtn').addClass("active");
        $('#dBtn').removeClass("active");
        $('.foodHistory').show();
        $('.dailyIntake').hide();
    });
</script>
</body>
</html>
