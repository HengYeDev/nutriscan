
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    String userId = null;
    if (request.getSession().getAttribute("userId") == null) {
        response.sendRedirect("/login");
    }

%>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.userway.org/widget.js" data-account="n8rLcGf9QH"></script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="NutriScan" />
    <title>Healthcare Professional Monitor - NutriScan</title>
    <!-- Favicon--><!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</head>
<body class="d-flex flex-column h-100">
<main class="flex-shrink-0">
    <!-- Navigation-->
    <%@ include file="jspf/nav.jspf" %>
    <!-- Header-->
    <header class="bg-success py-3">
        <div class="container px-5">
            <h1 class="text-light" id="title">Healthcare Professional Dashboard</h1>
        </div>
    </header>
    <section class="container px-5 mt-3" id="default">
        <!-- Features section-->
        <p>Welcome to the healthcare professional database! Here, you can monitor your patients' diet records.</p>
        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal" id="addButton">+ Add Client</button>
        <hr>
        <h4>My Clients</h4>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody id="content">

            </tbody>
        </table>
    </section>
</main>
<!-- Footer-->
<footer class="bg-dark py-4 mt-auto">
    <div class="container px-5">
        <div class="row align-items-center justify-content-between flex-column flex-sm-row">
            <div class="col-auto"><div class="small m-0 text-white">Copyright &copy; NutriScan 2024</div></div>
            <div class="col-auto">
                <a class="link-light small" href="#!">Privacy</a>
                <span class="text-white mx-1">&middot;</span>
                <a class="link-light small" href="#!">Terms</a>
                <span class="text-white mx-1">&middot;</span>
                <a class="link-light small" href="#!">Contact</a>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Client</h5>

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <label>
                        Client Name <br><input type="text" name="name" class="form-control w-100">
                    </label>
                    <br>
                    <label>
                        Client's NutriScan ID <br>
                        <input type="text" class="form-control w-100" name="code">
                    </label>
                    <hr>
                    <button type="button" class="btn btn-success" id="addBtn">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/qr-scanner/1.4.2/qr-scanner.umd.min.js" integrity="sha512-a/IwksuXdv0Q60tVkQpwMk5qY+6cJ0FJgi33lrrIddoFItTRiRfSdU1qogP3uYjgHfrGY7+AC+4LU4J+b9HcgQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>


    $('#addBtn').click(function() {
        $.post("/add-client", $('form').serialize(), function(data) {
           window.location = "client.jsp?id=" + data.rid;
        });
    });

    $.get("/relations", function(data) {
        for (var i = 0; i < data.length; i++) {
            $('#content').append("<tr><td>" + data[i].name + "</td><td><a class='btn btn-success' href='client.jsp?id=" + data[i].relationId + "'>View</a>");
        }
    })

</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
