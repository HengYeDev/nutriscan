<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%
    if (request.getSession().getAttribute("userId") == null) {
        response.sendRedirect("/login");
    }
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://cdn.userway.org/widget.js" data-account="n8rLcGf9QH"></script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="NutriScan" />
    <title>Scan - NutriScan</title>
    <!-- Favicon--><!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</head>
<body class="d-flex flex-column h-100">
<main class="flex-shrink-0">
    <!-- Navigation-->
    <%@ include file="jspf/nav.jspf" %>
    <!-- Header-->
    <header class="bg-success py-3">
        <div class="container px-5">
            <h1 class="text-light" id="title">Scan Now</h1>
        </div>
    </header>
    <section class="container px-5 mt-3" id="default">
        <!-- Features section-->
        <p>Please take a picture of your meal. </p>

        <form id="f">
            <input type="file" class="btn btn-primary btn-lg" accept="image/*" capture="camera" name="picture">
        </form>
        <hr>
        <button type="button" class="btn btn-success mt-1" id="upload">Upload</button>
    </section>
    <section class="container px-5 mt-3 d-none" id="loading">
        <div class="spinner-border text-success" role="status">
            <span class="visually-hidden">Loading...</span>
        </div> Processing with AI vision...
    </section>
    <section class="container px-5 mt-3 d-none" id="result">
        <p>Food item: <span id="food"></span></p>
        <h5>Calories: <span id="calories"></span></h5>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Nutrient</th>
                <th scope="col">Amount (g)</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <th scope="row">Protein</th>
                <td id="protein"></td>
            </tr>
            <tr>
                <th scope="row">Carbohydrates</th>
                <td id="carbohydrates"></td>
            </tr>
            <tr>
                <th scope="row">Fats</th>
                <td id="fats"></td>
            </tr>
            <tr>
                <th scope="row">Vitamins</th>
                <td id="vitamins"></td>
            </tr>
            <tr>
                <th scope="row">Minerals</th>
                <td id="minerals"></td>
            </tr>
            <tr>
                <th scope="row">Sodium</th>
                <td id="sodium"></td>
            </tr>
            </tbody>
        </table>
        <p><a href="profile.jsp">Back to dashboard</a></p>
    </section>
</main>
<script>
    $("#upload").click(function() {
        $('#default').addClass('d-none');
        $('#loading').removeClass('d-none');

        $.post({
            url: "/food-data",
            data: new FormData($('#f')[0]),
            processData: false,
            contentType: false,
            success: function (data) {

                if (data.error) {
                    data.food = data.error;
                }
                $('#loading').addClass("d-none")
                $('#result').removeClass('d-none');
                $('#title').text("Your meal");
                $('#food').text(data.food);
                $('#protein').text(data.protein);
                $('#carbohydrates').text(data.carbohydrates);
                $('#fats').text(data.fats);
                $('#vitamins').text(data.vitamins);
                $('#minerals').text(data.minerals);
                $('#calories').text(data.calories);
                $('#sodium').text(data.sodium/1000);

            }
        });
    });

</script>
<!-- Footer-->
<footer class="bg-dark py-4 mt-auto">
    <div class="container px-5">
        <div class="row align-items-center justify-content-between flex-column flex-sm-row">
            <div class="col-auto"><div class="small m-0 text-white">Copyright &copy; NutriScan 2024</div></div>
            <div class="col-auto">
                <a class="link-light small" href="#!">Privacy</a>
                <span class="text-white mx-1">&middot;</span>
                <a class="link-light small" href="#!">Terms</a>
                <span class="text-white mx-1">&middot;</span>
                <a class="link-light small" href="#!">Contact</a>
            </div>
        </div>
    </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>
